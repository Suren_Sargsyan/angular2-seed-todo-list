import {Component} from '@angular/core';

import { Todo } from './shared/todo';
// import { todos } from './shared/data.service';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css'],
})
export class AppCoponent {
    title: string = 'Angular 2do';
};
