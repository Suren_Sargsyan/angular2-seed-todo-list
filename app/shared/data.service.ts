import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const todos = [
            {
                id: 1,
                title: 'AAAAAAAAAAA',
                completed: true
            },
            {
                id: 2,
                title: 'CCCCCCCCCCC',
                completed: false
            },
            {
                id: 3,
                title: 'BBBBBBBBBBB',
                completed: false
            },
        ];

        return { todos }
    }
}
